use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;

// Define a struct to represent a city
#[derive(Debug, PartialEq)]
struct City {
    name: String,
}

impl City {
    // Function to create a new city from CSV record
    fn new(record: csv::StringRecord) -> City {
        City {
            name: record.get(0).unwrap_or("").to_string(),
        }
    }
}

// Function to read cities from CSV and return vector of cities
fn read_cities_from_csv(file_path: &str) -> Result<Vec<City>, Box<dyn Error>> {
    let mut cities = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let city = City::new(record);
        cities.push(city);
    }
    Ok(cities)
}

// Function to find city by name (case-sensitive)
fn find_city_by_name<'a>(cities: &'a [City], name: &str) -> Option<&'a City> {
    cities.iter().find(|c| c.name.to_lowercase() == name.to_lowercase())
}

fn main() {
    // Read cities from CSV
    let cities = match read_cities_from_csv("./data/cities.csv") {
        Ok(cits) => cits,
        Err(err) => {
            eprintln!("Error reading cities: {}", err);
            return;
        }
    };

    // Get city name from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <city_name>", args[0]);
        return;
    }
    let city_name = &args[1];

    // Find city by name and print information
    match find_city_by_name(&cities, city_name) {
        Some(city) => println!("{:?}", city),
        None => println!("City not found"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_city_by_name_existing() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(
            find_city_by_name(&cities, "New York"),
            Some(&City { name: "New York".to_string() })
        );
    }

    #[test]
    fn test_find_city_by_name_not_existing() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(find_city_by_name(&cities, "Chicago"), None);
    }

    #[test]
    fn test_find_city_by_name_case_insensitive() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(
            find_city_by_name(&cities, "new york"),
            Some(&City { name: "New York".to_string() })
        );
    }
}
