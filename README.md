# Rust Command-Line Tool with Testing

This project presents a Rust command-line tool for reading and processing data from a CSV file containing city names. It includes functionalities for reading city data from the CSV, searching for cities by name, and outputting city information.

The main features of this tool are:

- Reading city names from a specified CSV file.
- Searching for a city by name through the command-line interface.
- Displaying the information of the specified city if it exists in the CSV file.


## Project Setup
1. Create a new Rust project using the following command. Note I used the `--bin` flag to produce the binary executable for the command line tool later.
```bash
cargo new command-line-tool --bin
```
2. Prepare some data file used for the program. I used a csv file contained Star Wars characters and their information. Here is the format:
```csv
Name
New York
...
```
3. Add the following dependency to the `Cargo.toml` file.
```toml
[dependencies]
csv = "1.1.6"
```
4. Implement the program functionality in `main.rs`.

## Tool Functionality
After implementing the program, we can check the functionality of the tool. 
1. Build the program first using the following command:
```bash
cargo build
```

2. Run the following command with a String arguemnt, which is city name such as 'New York'.
```bash
./target/debug/command-line-tool 'New York'
```

Sample output:
![output](output.png)


## Testing implementation

### Detailed Steps
1. I implemented some unit tests to test the implementation. Here is an example. This unit test tests the functionality of finding a character whose name exists in the data.
```rust
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_city_by_name_existing() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(
            find_city_by_name(&cities, "New York"),
            Some(&City { name: "New York".to_string() })
        );
    }

    #[test]
    fn test_find_city_by_name_not_existing() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(find_city_by_name(&cities, "Chicago"), None);
    }

    #[test]
    fn test_find_city_by_name_case_insensitive() {
        let cities = vec![
            City { name: "New York".to_string() },
            City { name: "Los Angeles".to_string() },
        ];

        assert_eq!(
            find_city_by_name(&cities, "new york"),
            Some(&City { name: "New York".to_string() })
        );
    }
}
```

2. Run all the test cases on the terminal using the following command:
```bash
cargo test
```
### Testing Report

![report](report.png)
